-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: cmc_cpm_development
-- ------------------------------------------------------
-- Server version	5.6.24-0ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cpm_comments`
--

DROP TABLE IF EXISTS `cpm_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpm_user_id` int(11) DEFAULT NULL,
  `cpm_issue_id` int(11) DEFAULT NULL,
  `cpm_task_id` int(11) DEFAULT NULL,
  `cpm_project_id` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_comments`
--

LOCK TABLES `cpm_comments` WRITE;
/*!40000 ALTER TABLE `cpm_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_files`
--

DROP TABLE IF EXISTS `cpm_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpm_user_id` int(11) DEFAULT NULL,
  `cpm_task_id` int(11) DEFAULT NULL,
  `cpm_issue_id` int(11) DEFAULT NULL,
  `cpm_comment_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_files`
--

LOCK TABLES `cpm_files` WRITE;
/*!40000 ALTER TABLE `cpm_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_histories`
--

DROP TABLE IF EXISTS `cpm_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpm_user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpm_project_id` int(11) DEFAULT NULL,
  `cpm_task_id` int(11) DEFAULT NULL,
  `cpm_issue_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_histories`
--

LOCK TABLES `cpm_histories` WRITE;
/*!40000 ALTER TABLE `cpm_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_issues`
--

DROP TABLE IF EXISTS `cpm_issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpm_user_id` int(11) DEFAULT NULL,
  `cpm_project_id` int(11) DEFAULT NULL,
  `man_hour` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `cpm_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpm_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpm_process_id` int(11) DEFAULT NULL,
  `cpm_priority_id` int(11) DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_issues`
--

LOCK TABLES `cpm_issues` WRITE;
/*!40000 ALTER TABLE `cpm_issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_issues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_priorities`
--

DROP TABLE IF EXISTS `cpm_priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_priorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_priorities`
--

LOCK TABLES `cpm_priorities` WRITE;
/*!40000 ALTER TABLE `cpm_priorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_priorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_processes`
--

DROP TABLE IF EXISTS `cpm_processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_processes`
--

LOCK TABLES `cpm_processes` WRITE;
/*!40000 ALTER TABLE `cpm_processes` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_processes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_projects`
--

DROP TABLE IF EXISTS `cpm_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpm_user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_projects`
--

LOCK TABLES `cpm_projects` WRITE;
/*!40000 ALTER TABLE `cpm_projects` DISABLE KEYS */;
INSERT INTO `cpm_projects` VALUES (6,1,'Goseedoc',NULL,NULL,1,'2015-07-01 07:08:05','2015-07-01 07:32:39'),(7,1,'44444',NULL,NULL,1,'2015-07-01 08:44:59','2015-07-01 10:48:15'),(10,NULL,'4',NULL,NULL,NULL,'2015-07-01 08:45:01','2015-07-01 08:45:01'),(12,NULL,'4',NULL,NULL,NULL,'2015-07-01 08:45:01','2015-07-01 08:45:01'),(13,4,'454545',NULL,NULL,4,'2015-07-01 10:11:54','2015-07-01 10:11:54'),(14,1,'323',NULL,NULL,12,'2015-07-01 10:16:23','2015-07-01 10:16:23'),(15,1,'343',NULL,NULL,1,'2015-07-01 10:27:28','2015-07-01 10:27:28'),(16,1,'2323',NULL,NULL,1,'2015-07-01 10:36:49','2015-07-01 10:36:49'),(17,1,'3',NULL,NULL,1,'2015-07-01 10:40:54','2015-07-01 10:40:54'),(18,1,'3',NULL,NULL,1,'2015-07-01 10:41:02','2015-07-01 10:41:02'),(19,1,'3',NULL,NULL,1,'2015-07-01 10:41:07','2015-07-01 10:41:07'),(20,1,'11111test',NULL,NULL,1,'2015-07-01 10:48:28','2015-07-01 10:48:28');
/*!40000 ALTER TABLE `cpm_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_projects_users`
--

DROP TABLE IF EXISTS `cpm_projects_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_projects_users` (
  `cpm_project_id` int(11) NOT NULL,
  `cpm_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_projects_users`
--

LOCK TABLES `cpm_projects_users` WRITE;
/*!40000 ALTER TABLE `cpm_projects_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_projects_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_tasks`
--

DROP TABLE IF EXISTS `cpm_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpm_user_id` int(11) DEFAULT NULL,
  `cpm_project_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `cpm_process_id` int(11) DEFAULT NULL,
  `cpm_priority_id` int(11) DEFAULT NULL,
  `man_hour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_tasks`
--

LOCK TABLES `cpm_tasks` WRITE;
/*!40000 ALTER TABLE `cpm_tasks` DISABLE KEYS */;
INSERT INTO `cpm_tasks` VALUES (7,1,NULL,'3',NULL,NULL,NULL,NULL,'344','2015-07-01 09:21:10','2015-07-01 09:31:11'),(22,1,NULL,'3',NULL,NULL,1,1,'23','2015-07-01 09:21:16','2015-07-01 09:21:37'),(25,1,NULL,'3',NULL,NULL,NULL,NULL,'44','2015-07-01 09:21:18','2015-07-01 09:30:54'),(29,1,NULL,'1111',NULL,NULL,1,2,'3','2015-07-01 09:52:56','2015-07-01 09:52:56'),(30,1,NULL,'3334',NULL,NULL,3,43,'333','2015-07-01 09:54:27','2015-07-01 09:54:27'),(31,1,NULL,'22',NULL,NULL,21,1,'44','2015-07-01 09:57:43','2015-07-01 10:04:37'),(32,1,NULL,'434',NULL,NULL,3,3,'44','2015-07-01 10:20:43','2015-07-01 10:20:43'),(34,1,NULL,'34444444444444',NULL,NULL,1,1,'3333','2015-07-01 10:44:02','2015-07-01 10:44:02');
/*!40000 ALTER TABLE `cpm_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_teams`
--

DROP TABLE IF EXISTS `cpm_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_teams`
--

LOCK TABLES `cpm_teams` WRITE;
/*!40000 ALTER TABLE `cpm_teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpm_users`
--

DROP TABLE IF EXISTS `cpm_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpm_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_digest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `your_story` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `cpm_team_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpm_users`
--

LOCK TABLES `cpm_users` WRITE;
/*!40000 ALTER TABLE `cpm_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpm_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,'5',4,'2015-07-01 02:25:14','2015-07-01 02:31:04'),(3,'444',54,'2015-07-01 02:31:23','2015-07-01 02:31:28'),(4,'3',3,'2015-07-01 02:32:12','2015-07-01 02:32:12'),(6,'545445',4,'2015-07-01 02:46:35','2015-07-01 02:46:35'),(7,'545445',4,'2015-07-01 02:49:09','2015-07-01 02:49:09'),(8,'test',1,'2015-07-01 03:19:03','2015-07-01 03:19:03'),(9,'444',4444,'2015-07-01 03:22:41','2015-07-01 03:22:41'),(10,'5',5,'2015-07-01 03:53:22','2015-07-01 03:53:22'),(11,'test',30,'2015-07-01 03:56:59','2015-07-01 03:56:59'),(12,'44',44,'2015-07-01 03:57:35','2015-07-01 03:57:35'),(13,'1',1,'2015-07-01 04:39:44','2015-07-01 04:39:44');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20150625023403'),('20150625023539'),('20150625023547'),('20150625023554'),('20150625023601'),('20150625023623'),('20150625023633'),('20150625023649'),('20150625023655'),('20150625023701'),('20150630101327'),('20150701021959');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-01 21:00:34
