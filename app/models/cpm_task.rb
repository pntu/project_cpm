class CpmTask < ActiveRecord::Base
  belongs_to :cpm_project
  belongs_to :cpm_user
  belongs_to :cpm_process
  belongs_to :cpm_priority
  has_many :cpm_comments

  attr_accessible :name, :cpm_user_id, :cpm_process_id,:cpm_priority_id,:man_hour
  validates :name, presence: true

end
