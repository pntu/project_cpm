class CpmProjectsController < ApplicationController
  layout 'default/application'
  respond_to :html, :js
  before_action only: [:index, :create,:edit,:update,:new]

  def index
    @products = CpmProject.all

  end

  def show
    @products = CpmProject.find(params[:id])
  end

  def details

    @cpm_project = CpmProject.find(params[:cpm_project_id])
    session[:cpm_project_id] = params[:cpm_project_id]
    session[:cpm_project_name] = @cpm_project.name
  end

  def new
    @products = CpmProject.new
    respond_to do |format|
      format.js   {}
    end
  end

  def create
    @products = CpmProject.all
    @products = CpmProject.create(product_params)

    respond_to do |format|
      format.js   {}
    end
  end

  def edit
    @products = CpmProject.find(params[:id])
  end

  def update
    @products = CpmProject.all
    @products = CpmProject.find(params[:id])
    @products.update_attributes(product_params)
  end

  def delete
    @products = CpmProject.find(params[:id])
  end

  def destroy
    @products = CpmProject.all
    @products = CpmProject.find(params[:id])
    @products.destroy
  end

  private
  def product_params
    params.require(:cpm_project).permit(:name,:start_date,:end_date,:cpm_user_id,:status)
  end

end
