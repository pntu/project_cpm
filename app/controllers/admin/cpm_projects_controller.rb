class Admin::CpmProjectsController < ApplicationController
  layout 'default/application'
  respond_to :html, :js

  def index
    @arrData = CpmProject.paginate(:page => params[:page], :per_page => 10)
    @search = CpmProject.new
  end

  def show
    @data = CpmProject.find(params[:id])
  end

  def new
    @data = CpmProject.new
  end

  def create
    @arrData = CpmProject.paginate(:page => params[:page], :per_page => 10)
    @data = CpmProject.create(data_params)
  end

  def edit
    @data = CpmProject.find(params[:id])
  end

  def update
    @arrData = CpmProject.paginate(:page => params[:page], :per_page => 10)
    @data = CpmProject.find(params[:id])
    @data.update_attributes(data_params)
  end

  def delete
    @data = CpmProject.find(params[:cpm_project_id])
  end

  def destroy
    @arrData = CpmProject.paginate(:page => params[:page], :per_page => 10)
    @data = CpmProject.find(params[:id])
    @data.destroy
  end

  private
  def data_params
    params.require(:cpm_project).permit(:name,:start_date,:end_date,:cpm_user_id,:status)
  end
end
