class CpmTasksController < ApplicationController
  layout 'default/application'
  respond_to :html, :js

  def index
    @arrData = CpmTask.paginate(:page => params[:page], :per_page => 10)
    @task = CpmTask.new
  end

  def search
    @data = CpmTask.new
    @arrData = CpmTask.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @data = CpmTask.find(params[:id])
  end

  def new
    @data = CpmTask.new
  end

  def create
    @arrData = CpmTask.paginate(:page => params[:page], :per_page => 10)
    params[:cpm_task][:cpm_user_id] = 1
    @data = CpmTask.create(data_params)
  end

  def edit
    @data = CpmTask.find(params[:id])
  end

  def update
    @arrData = CpmTask.paginate(:page => params[:page], :per_page => 10)
    @data = CpmTask.find(params[:id])
    @data.update_attributes(data_params)
  end

  def delete
    @data = CpmTask.find(params[:cpm_task_id])
  end

  def destroy
    @arrData = CpmTask.paginate(:page => params[:page], :per_page => 10)
    @data = CpmTask.find(params[:id])
    @data.destroy
  end

  private
  def data_params
    params.require(:cpm_task).permit(:name,:cpm_user_id,:deadline,:cpm_process_id,:cpm_priority_id,:man_hour)
  end
end
