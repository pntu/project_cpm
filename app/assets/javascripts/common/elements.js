

//IOS Check box
(function(e){e.fn.extend({iosCheckbox:function(){e(this).each(function(){var t=e(this);var n=jQuery("<div>",{"class":"ios-ui-select"}).append(jQuery("<div>",{"class":"inner"}));if(t.is(":checked")){n.addClass("checked")}t.hide().after(n);n.click(function(){n.toggleClass("checked");if(n.hasClass("checked")){t.prop("checked",true)}else{t.prop("checked",false)}})})}})})(jQuery)
