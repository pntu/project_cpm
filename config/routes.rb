Rails.application.routes.draw do
  namespace :admin do
    resources :cpm_projects do
      get 'delete'
    end
  end

  resources :products do
    get "delete"
  end
  resources :cpm_users


  namespace :admin do
    get 'index/index'
    get ''=>'index#index', :as=>'index'
    get 'index'=>'index#index', :as=>'admin_index'
  end

  resources :cpm_processes
  resources :cpm_priorities
  resources :cpm_issues
  resources :cpm_teams
  resources :cpm_histories
  resources :cpm_files

  resources :cpm_projects do
    get 'delete'
    get '/show' =>'cpm_projects#details', :as=> 'get_cpm_projects_show'
  end

  resources :cpm_tasks do
    get 'delete'
  end
  post 'cpm_tasks/search'=>'cpm_tasks#search'


  resources :cpm_comments
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'dashboard#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'


  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  # 
  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     #   end
end
